
<!DOCTYPE html>
<html>
	<head>
		<title>Queue Management System</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"></script>
	</head>
		<body>
		

			<div class="container-fluid" style="background-image: url(upload/user_login.png); background-repeat: no-repeat;background-size: cover; height: 100vh;">
				<div class="row col-12">
					<?php include 'includes/navbar.php';?>
				</div>

				<div class="row col-12">
				</div>
					<div class="d-flex align-items-end justify-content-end" style="height: 50vh;">
						<form class="panel-group form-horizontal col-4 text-center" action="accounts/login.php" role="form">
	    					<div class="panel panel-default">
		    					<div class="panel-body">
								<div class="panel-header">
									<br>
									<h2>Registered Users Login</h2>
                     
								</div>
								<div class="input-group">
							
									<input type="text" name="user_name" class="form-control" placeholder="User Name" required>
								</div>	
								<div class="input-group">
									<input type="password" name="user_password" class="form-control" placeholder="Password" required>
								</div>
                               	
							    <div class="input-group-btn">
									<button class="btn btn-success btn-block" name="submit" type="submit">Login</button>
								</div>
								<div class="input-group-label">
									<h3>Having Problem in login,Kindly Contact Administrator</h3>
								</div>

								
								<div>
										<?php

										if(isset($_GET["login_error"]))
										{
											echo
											'
											<h3 class="bg-danger">'.$_GET["login_error"].'</h3>
											';

											
										}
										?>
								
								</div>
							</div>
                                
							</div>
							
						
					</form>

					</div>
					
				
				</div>
				<div class="row">
					<?php include 'includes/footer.php';?>
				</div>
			</div>
	
		
    					
		</body>
</html>
