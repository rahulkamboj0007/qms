<?php session_start();
    include '../includes/db.php';
    $error= '';
    if(isset($_SESSION['user']) && isset($_SESSION['password']) == true)
    {
        if($_SESSION['role'] == 'user')
        {
         
        }
        else {
            header('Location:../account.php');
        }
    }    
    else
    {
        header('Location:../account.php');
    }
	
?>


<!DOCTYPE html>
<html>
	<head>
		<title>Queue Management System</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	</head>
		<body>
                <div class="container-fluid" style="background-image: url(../upload/user_back.jpg); background-repeat: no-repeat;background-size: cover; height: 100vh;>
                     <?php include "navbar.php"?>
                                    
                    <div class="body">
                    
                        <div class="row">
                            
                        </div>
                        <div class="row">
                        <div class="col-lg-3"><?php include "sidebar.php"?></div>
                        <div class="col-sm-6" style="background-color: rgba(0, 0, 0, 0.25); ">
                        
                            
                            <form action="">

                                        <table class="table table-hover text-center table-dark">
                                        <thead>
                                        <tr>
                                            <th colspan="5">Below Token is in your Queue</th>
                                                                                    </tr>
                                        <tr>
                                        
                                            <th>Token Number</th>
                                            <th>Citizen Name</th>
                                            <th>Service</th>
                                            <th>Action</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        
                                        
                                        <?php
                                                 $id = $_SESSION['id'];
                                                  
                                                 $sql_u = "SELECT * 
                                                 from citizen_token
                                                 inner join service
                                                 on citizen_token.service = service.s_id
                                                  where status = 'waiting' AND t_assign = '$id' order by t_number";
                                                 $result_u = mysqli_query($conn,$sql_u);
                                                 while($row_u = mysqli_fetch_assoc($result_u)){
                                                    $t_id = $row_u['t_id'];
                                                    $t_number = $row_u['t_number'];
                                                    $first_name = $row_u['first_name'];
                                                    $last_name = $row_u['last_name'];
                                                    $service = $row_u['service'];

                                                    echo '

                                                    <tr>
                                           
                                            <td>'.$t_number.'</td>
                                            <td>'.$first_name.' '.$last_name.' </td>
                                            <td>'.$service.'</td>
                                            <td><a href="done_token.php?t_id='.$t_id.'" class="btn btn-danger" role="button">Done</a></td>
                                            <td><a href="reopen_token.php?t_id='.$t_id.'" class="btn btn-warning" role="button">Re-Open</a></td>
                                            

                                                    ';
                                                   
                                                    
                                                  }
                                                                                            
                                            
                                            ?>
                                            
                                        </tr>
                                        <tr>
                                        <td colspan="5"><a href="start_token.php" class="btn btn-success btn-block" role="button">Next</a></td>
                                        </tr>
                                        <tr>
                                        <td colspan="5"><?php if(isset($_GET['error'])){ echo $_GET["error"];} ?></td>
                                        </tr>
                                       </tbody>
                                    </table>
                            
                            
                            
                            </form>                        
                        
                        </div>
                        <div class="col-sm-4"></div>
                        
                        </div>
                        </div>
                        

                 
                  
                </div>    
				
			
  
                    
				
		</body>
</html>
