<!DOCTYPE html>
<html lang="en">
<head>
  <title>Queue Management System</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
  <div class="row">
        <div class="col-12 fixed-top mb-lg-auto">
            <?php include 'includes/navbar.php';?>
        </div>
  </div>     
      <div class="row">
        <div class="col-12" style="background-image: url(upload/queue-management-features.png); width: 100%; height: 100vh; background-size: cover; background-position: center;">
            <div class="d-flex justify-content-center align-items-center" >
                
                <?php 
							
							include 'includes/db.php';
							$date = date("d-m-Y");
							$sql = "SELECT * FROM citizen_token where status = 'Waiting' AND t_date = '$date'";
							$result = mysqli_query($conn, $sql);
							while($row = mysqli_fetch_assoc($result)) {
								//echo "Token No. = " . $row["t_number"]. " - Citizen Name: " . $row["first_name"]. " " . $row["last_name"]. "<br>";
							}
	
						?>
					<div class="col-4 mt-lg-5">
								
						<form class="panel-group form-horizontal mt-lg-5" action="gen_token.php" role="form" style="background-color: rgba(0, 0, 0, 0.151);">
							<img  src="upload/Logo.png"  style="width: 100px; display: block; margin-left: auto; margin-right: auto;">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="panel-header">
									
                                        <h4 style="text-align: center; text-shadow: azure;;">Generate Token For Your Turn</h4>
                                    </div>
                                    <div class="input-group">
                                        <input type="text" name="f_name" class="form-control" placeholder="First Name" required>
                                        <input type="text" name="l_name" class="form-control" placeholder="Last Name" required>
                                    </div>	
                                    <div class="input-group">
                                        <input type="text" pattern="[789][0-9]{9}" name="mobile" class="form-control" placeholder="Mobile No." required>
                                    </div>
                                            <div class="input-group"> <div>Location</div>
                                             <select class="input-group" name="location" required>
                                             <option value="">Select Location</option>
                                            <?php 
                                             include '../includes/db.php';
                                             $sql = "SELECT * FROM location";
                                             $result = mysqli_query($conn, $sql);
                                             while($row = mysqli_fetch_assoc($result)) {
                                             
                                             echo '
                                          
                                                <option value='.$row["loc_id"].'>'. $row["location"].'</option>
                                             
                                               ';
    
                                             }
                                            ?> 
                                            </select>
                                            </div>
                                            <div class="input-group"> <div>Service</div>
                                             <select class="input-group" name="service" required>
                                             <option value="">Select Required Service</option>
                                            <?php 
                                             include 'includes/db.php';
                                             $sql = "SELECT * FROM service";
                                             $result = mysqli_query($conn, $sql);
                                             while($row = mysqli_fetch_assoc($result)) {
                                             
                                             echo '
                                          
                                             <option value='.$row["s_id"].'>'. $row["service"].'</option>
                                            ';
    
                                             }
                                            ?> 
                                            </select>
                                            </div>
                                            
                                        <div class="input-group-btn">
                                        <br>
                                        <button class="btn btn-success btn-block" name="submit" type="submit">Generate</button><br>
                                    </div>
                                    <div id='printMe'>
                                    <?php 
                                    if(isset($_GET["msg"])){
										
                                        echo '
										<div class="bg-warning text-center">
											<h2>Your Token Number : '.$_GET["msg"].'</h2>
											<br><h3>Kindly Wait For Your Turn</h3>

										</div>
									
										';
                                      
                                        echo '<button  class = "btn btn-warning btn-block" onclick="window.print()">Print only if neccessary<br></button><br>';
                                    }
									if(isset($_GET["error"])){
                                        echo '
										<div class="bg-warning text-center">
										<h3> '.$_GET["error"].'</h3>
									</div>
										';
                                       
                                        
                                    }
                                        
                                    ?>
                                            
                                    </div>
                                </div>
                            </div>
                        </form>   
					</div>	    

            </div>
        
        </div>    
    </div>
    <div class="row">
        <div class="col-12 fixed-bottom">
            <?php include 'includes/footer.php';?>
        </div>
  </div> 
    
    </div>
    

</body>
</html>
