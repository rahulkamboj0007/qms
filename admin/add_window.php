<?php
include '../includes/db.php';
if(isset($_GET['submit'])){
    $loc = $_GET['window'];
    $loc_id = $_GET['location'];
              $sql_int = "INSERT into window (window, loc_id) 
                     VALUES ('$loc','$loc_id')";
            if (mysqli_query($conn, $sql_int)) {
            echo "Window Added successfully";
            
        
        } else {
            echo "Error: " . $sql_int . "<br>" . mysqli_error($conn);
          }
          
        }

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Queue Management System</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	</head>
		<body>
		<?php include '../includes/navbar.php';?>
				<div class="container">
					<div class="row">
						<div class='col-4'>
					         <?php include 'sidebar.php';?>
						</div>
						<div class='col-4'>
                            <h4>Add Window for Queue Management System</h4>
					                <div><form class="panel-group form-horizontal" action="#" role="form">
						                <div class="panel panel-default">
							                <div class="panel-body">
								                <div class="panel-header">
								<!-------Select Location----->      

                                <div class="input-group"> <div>Select Location</div>
                                         <select class="input-group" name="location">
                                        <?php 
                                         include '../includes/db.php';
                                         $sql = "SELECT * FROM location";
                                         $result = mysqli_query($conn, $sql);
                                         while($row = mysqli_fetch_assoc($result)) {
                                         
                                         echo '
                                      
                                            <option value='.$row["loc_id"].'>'. $row["location"].'</option>
                                         
                                           ';

                                         }
                                        ?> 
                                        </select>                           

						    </div>


								    </div>
								    <div class="input-group">
									    <input type="text" name="window" class="form-control" placeholder="Add Window" required>
								    </div>	
									<div class="input-group-btn">
									    <button class="btn btn-success" name="submit" type="submit">Submit</button>
								    </div>
							        </div>
                            
                        </div>
                        </div>
                        </div>
						<div class='col-4'>

                                    <?php
                                  
                                  include '../includes/db.php';
                                  $sql = "SELECT * FROM window";
                                  $result = mysqli_query($conn, $sql);
                                  while($row = mysqli_fetch_assoc($result)) {
                                      echo "ID = " . $row["w_id"]. " - Window " . $row["window"]." Location ". $row["loc_id"]."<br>";
                                  }

                                    ?>

						</div>
						
					<div class="row">
					<div style="width:50px;height:50px;"></div>
					<?php include '../includes/footer.php';?>
					</div>
				</div>
				</div>
				
		</body>
</html>
